# Продолжите писать анализаторы для текста. Теперь необходимо реализовать код, с помощью которого можно определять палиндромы. То есть нужно находить слова, которые одинаково читается слева направо и справа налево.
#
# Напишите такую программу.
#
# Пример 1:
#
# Введите слово: мадам
#
# Слово является палиндромом
#
# Пример 2:
#
# Введите слово: abccba
# Слово является палиндромом
#
# Пример 3:
#
# Введите слово: abbd
#
# Слово не является палиндромом

revers = ""
world = input("Введите слово: ")
for i in reversed(world):
    revers += i
print(revers)
if world == revers:
    print("Это палиндром")
else:
    print("Это не палиндром")
